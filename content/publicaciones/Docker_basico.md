---
title: "Fundamentos de Docker"
date: 2021-01-19T14:41:51-03:00
draft: false
---

![Screenshot](/images/docker1.png)

# Fundamentos de Docker

Fue lanzado el 13 de marzo del 2013. Projecto creado por  Solomon Hykes, Docker es un proyecto de código abierto que automatiza el despliegue de aplicaciones dentro de contenedores de software, proporcionando una capa adicional de abstracción y automatización de virtualización de aplicaciones en múltiples sistemas operativos.

En simple, la idea detrás de Docker es crear contenedores ligeros y portables para las aplicaciones software que puedan ejecutarse en cualquier máquina con Docker instalado, independientemente del sistema operativo que la máquina tenga por debajo, facilitando así también los despliegues.

Docker utiliza caracteristicas nativas de linux para funcionar (vease cgroup y  espacios de nombres (namespaces) ) y aislar y ejecutar de forma independiente en una sola instancia de linux. El soporte del kernel Linux para los espacios de nombres aísla la vista que tiene una aplicación de su entorno operativo, incluyendo árboles de proceso, red, ID de usuario y sistemas de archivos montados, mientras que los cgroups del kernel proporcionan aislamiento de recursos, incluyendo la CPU, la memoria, el bloque de E/S y de la red.

## Un ejemplo simplica todo, cierto ?

Sin utilizar Docker, un posible caso podria ser:

* Mauricio esta programando una funcionalidad especifica que solo esta disponible en Java 11.
* Marcelo tiene instalado Java 9 y trabaja en otros projectos. Mauricio quiere que Marcelo ejecute el codigo en su maquina. Marcelo tendra que instalae Java 11 o la aplicación fallara en su maquina.

Con Docker este escenario desaparece. Para ejecutar esta aplicación, Mauricio crea un contenedor de docker con la aplicación en Java 11 y los recursos necesarios y lo entrega a Marcelo.

Marcelo, teniendo instalado Docker en su maquina, puede ejecutar la aplicación a traves de un contenedor, sin tener que instalar nada.

Con lo anterior podemos deducir algunos beneficios:

* Docker es muy bueno para entornos de prueba. Es muy sencillo crear y borrar un contenedores, ademas que son livianos y se pueden ejecutar varios a la ves en un misma maquina ( contenedores de base de daros, servidor, librerias). Por otro lado, un mismo contenedor funcionará en cualquier maquina: un notebook, servidor, maquinas en la nube etc.
* Los contenedores son mas ligeros que las maquinas virtuales, por lo tanto se puede ahorra en recursos de infraestructura.
* Docker es Open Source.

## Contenedores vs Maquinas virtuales

El concepto es similar, pero no es lo mismo. Un contenedor es mas ligero, ya que una maquina virtual necesitas de un sistema operativo para funcionar, un contenedor funciona utilizando el sistema operativo que tiene la maquina que ejecutar ese contenedor. 

El concepto de contenedor es similar a la de una JVM. Que un contenedor Docker tome los aspectos mas basicos del funcionamiento de un sistemas operativo de la maquina que lo ejecuta lo vuelve mas ligero que una maquina virtual.

La siguiente imagen facilita la compresión de contenedores vs maquinas virtuales.

![Screenshot](/images/vms_vs_container.jpg)


## Teoria! Docker Engine

Es una aplicación cliente-servidor y maneja estos componentes principales

* Un servidor que es un tipo de programa de larga duración llamado proceso daemon (el comando dockerd).
* Una API REST que especifica las interfaces que los programas pueden usar para hablar con el demonio e indicarle qué hacer.
* Un cliente de interfaz de línea de comando (CLI) (el comando docker).

La siguiente imagen representa los conceptos explicados:

![Screenshot](/images/docker_engine.png)


## La arquitectura de Docker

Como se menciono, Docker utiliza una arquitectura cliente-servidor. La imagen representa las arquitectura de Docker. El cliente docker habla con el Docker Daemon, que se encarga de contruir, ejecutar y distribuir los contenedores en Docker. Tanto el cliente Docker como Docker Daemon puede ejecutarse en un mismo sistema, como separados. La interacción entre el cliente y el Daemon es a traves de la API REST.


![Screenshot](/images/architecture.svg)


De la imagen podemos destacar:

* **El demonio Docker**

El daemon de Docker (dockerd) escucha las solicitudes de la API de Docker y administra los objetos de Docker, como imágenes, contenedores, redes y volúmenes. Un demonio también puede comunicarse con otros demonios para administrar los servicios de Docker.

* **El cliente Docker**

El cliente Docker (docker) es la forma principal en que muchos usuarios de Docker interactúan con Docker. Cuando utiliza comandos como Docker Run, el cliente envía estos comandos a Dockerd, que los ejecuta. El comando docker usa la API Docker. El cliente Docker puede comunicarse con más de un demonio.

* **Registros de Docker**

Un registro de Docker almacena imágenes de Docker. Docker Hub es un registro público que cualquiera puede usar, y Docker está configurado para buscar imágenes en Docker Hub de manera predeterminada. Incluso puede ejecutar su propio registro privado.

Cuando utiliza los comandos docker pull o docker run, las imágenes requeridas se extraen de su registro configurado. Cuando utiliza el comando push docker, su imagen se inserta en su registro configurado.

* **Imagenes**

Una imagen es una plantilla de solo lectura con instrucciones para crear un contenedor Docker. A menudo, una imagen se basa en otra imagen, con alguna personalización adicional. Por ejemplo, puede crear una imagen basada en la imagen de ubuntu, pero instala el servidor web Apache y su aplicación, así como los detalles de configuración necesarios para que su aplicación se ejecute.

Puede crear sus propias imagenes o usar las imagenes de Docker hub. Para crear una imagen debe crear un dockerfile con los pasos que necesite y ejecutarla.

* **Contenedores**

Un contenedor es una instancia ejecutable de una imagen. Puede crear, iniciar, detener, mover o eliminar un contenedor utilizando la API o CLI de Docker. Puede conectar un contenedor a una o más redes, adjuntarle almacenamiento o incluso crear una nueva imagen en función de su estado actual.


Una vez entendido los componentes y su interacción, es momento de pasar a la acción!:

## 1. Instalando Docker

Para instalar docker, necesitamos agregar el repositorio de docker en nuestro servidor:

* Agregue la GPG key de docker

```
$ sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

* Agregue el repositorio

```
$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable edge"
```

* Actualice la base de datos de paquetes:

```
$ sudo apt-get update
$ sudo apt-cache policy docker-ce
```

* Instale docker

```
$ sudo apt-get install -y docker-ce
```

* valide que el servicio este iniciado

```
$ sudo systemctl status docker
```

Ahora valide su instalación ejecutando su primera imagen!:

```
$ sudo docker run hello-world
```

La salida:

```
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
0e03bdcc26d7: Pull complete 
Digest: sha256:6a65f928fb91fcfbc963f7aa6d57c8eeb426ad9a20c7ee045538ef34847f44f1
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/

```

La primera linea indica que la imagen 'hello-world:latest' no se encuentra en su servidr, por lo tanto sea descargada por defecto desde Docker hub.

Si requiere utilizar docker con usuario nominales, solo debe agregar el usuario al grupo docker:

```
$ sudo usermod -aG docker [usuario]
```

Cierre sesión del usuario y vuelva a ingresar para que los cambios funcionen.

La instalación esta lista!!!

## 2. Usando el comando Docker

La sintaxis del comando:

```
$ docker [option] [command] [arguments]
```

Para visualizar los subcomandos disponibles ejecutar ```docker``` sin argumentos.

Si desea ver las opciones disponibles para un comando especifico:

```
$ docker docker-subcommand --help
```

## 3. Trabajando con imagenes:

Para este ejemplo se utilizara una imagen de ubuntu. Para buscar las imagenes disponibles:

```
$ docker search ubuntu
```

Por defecto docker buscara en Docker hub las imagenes que coincidan con "ubuntu". La salida entrega la columna "OFFICIAL" y corresponde a images creadas y soportadas por la empresa que respalda el proyecto:

```
NAME                                                      DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
ubuntu                                                    Ubuntu is a Debian-based Linux operating sys…   10975               [OK]                
dorowu/ubuntu-desktop-lxde-vnc                            Docker image to provide HTML5 VNC interface …   435                                     [OK]
rastasheep/ubuntu-sshd                                    Dockerized SSH service, built on top of offi…   244                                     [OK]
```

Para descargar la imagen oficial a su servidor, ejecute:

```
$ docker pull ubuntu
```

La salida:

```
Using default tag: latest
latest: Pulling from library/ubuntu
d51af753c3d3: Pull complete 
fc878cd0a91c: Pull complete 
6154df8ff988: Pull complete 
fee5db0ff82f: Pull complete 
Digest: sha256:747d2dbbaaee995098c9792d99bd333c6783ce56150d1b11e333bbceed5c54d7
Status: Downloaded newer image for ubuntu:latest
docker.io/library/ubuntu:latest

```

La imagen descargada puede visualizar con el comando:

```
$ docker images
```

La salida:

```
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
ubuntu              latest              1d622ef86b13        6 weeks ago         73.9MB
hello-world         latest              bf756fb1ae65        5 months ago        13.3kB
```

## 4. Ejecutando un contenedor 

Ya tenemos la imagen ubuntu lista, ahora la ejecutaremos en modo interactivo:

```
$ docker run -ti ubuntu
```

* Notará que su linea de comandos cambiará:

```
root@d9041dcfcb98:/#
```

* Ya esta dentro de un contenedor!, dentro de el puede ejecutar comandos, instalar, levantar servicios etc. Por ejemplo, actualicemos los repositorios:

```
# apt-get update
```

* Instale nginx:

```
# apt-get install nginx
# apt-get install curl
```

* inicie y valide el servicio:

```
# nginx
# curl 127.0.0.1
```

Salga del contenidor con "exit".

Tambien es posible enviar variable de entorno en la ejecución del contenedor, por ejemplo:

```
$ docker run -ti -e bla="hola" ubuntu
root@36f107278aa2:/# echo $bla
hola
```




## 5. Gestione los contenedores

Ahora aprenderemos a utilizar los comados basicos para la gestión de contenedores:

* **Visualizar contenedores activos**

```
$ docker ps
```

Como ve, no hay contenedores en ejecución. Para visualizar los contenedores activos como inactivos:

```
$ docker ps -a
```

La salida:

```
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                      PORTS               NAMES
d9041dcfcb98        ubuntu              "/bin/bash"         19 minutes ago      Exited (0) 3 minutes ago                        nifty_wing
aaa18a4d974a        hello-world         "/hello"            58 minutes ago      Exited (0) 58 minutes ago                       goofy_bhaskara
```

* **Iniciar un contenedor que ha sido detenido**

Para iniciar la imagen "ubuntu", utilice el container id:

```
$ docker start d9041dcfcb98
```

Valide con el comandos ```docker ps```:

```
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                  PORTS               NAMES
d9041dcfcb98        ubuntu              "/bin/bash"         22 minutes ago      Up Less than a second                       nifty_wing
```

* **Detener un contenedor**

Para detener puede utilizar el container id o el nombre del contenedor

```
$ docker stop nifty_wing
```

* **Eliminar contenedor**

Si no necesita utilizar mas el contenedor, puede eliminarlo:

```
$ docker rm d9041dcfcb98
```

* **Eliminar imagen**

Puede eliminar la imagen, siempre que no se este utilizando en algun contenedor:

```
$ docker rmi ubuntu
```

Puede validar con "docker images"

## 6. Creando un Dockerfile

Es un archivo de configuración que se utiliza para crear imágenes. En dicho archivo indicamos qué es lo que queremos que tenga la imagen, y los distintos comandos para instalar las herramientas. 

Ahora, crearemos un Dockerfile muy simple

* Cree una carpeta e ingrese a ella:

```
mkdir docker; cd docker
```

* cree un archivo llamado "Dockerfile"

```
touch Dockerfile
```

Edite el contenido del archivo y agrege lo siguiente:

```
FROM ubuntu
RUN apt-get update -y && apt-get install nginx -y
ENTRYPOINT echo "El dockerfile ha funcionado!"
```

Cada instrucción se escribe con mayuscula y crea una capa. Cada capa ejecuta:

* FROM = Utiliza la imagen "ubuntu" que ya descargamos. Si utiliza una imagen que no existe localmente, la descagará de docker hub.
* RUN = Ejecuta un comando durante el proceso de creación del contenedor. Para el ejemplo, actualiza los repositorios y luego instala nginx.
* ENTRYPOINT = Es una instrucción para que un comando se ejecute dentro del contenedor, justo después de que el contenedor sea ejecutado.

Ahora comprendido los puntos, contruyamos la imagen. El comando a utilizar el "docker build"

```
$ docker build -t demo-ubuntu:1.0 .
```

La opción "-t" agrega un TAG a la imagen. Para el ejemplo el nombre sera "demo-ubuntu" y su versión es "1.0". El "." contruira la imagen desde la carpeta donde estamos parados, en el carpeta actual solo tenemos el "Dockerfile".

Docker ejecuta cada instrucción del Dockerfile y aparecerá en pantalla, una vez terminado podra visualar la nueva imagen con el comando:

```
$ docker images
```

La salida:

```
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
demo-ubuntu         1.0                 4fdca8fe208a        16 minutes ago      155MB
ubuntu              latest              1d622ef86b13        6 weeks ago         73.9MB
hello-world         latest              bf756fb1ae65        5 months ago        13.3kB
```

Ahora ya podemos ejecutar nuestra imagen!. Ejecute:

```
docker run -ti --name test-build demo-ubuntu:1.0
```

--name = Puede asignar un nombre al contenedor

Aparecera el mensaje **"El dockerfile ha funcionado!"** y el contenedor se termina porque no tiene mas instrucciones en "ENTRYPOINT".

Ahora que el Dockerfile funciona, probemos el servidor web que ofrece nginx.

Editamos el Dockerfile, quitamos el ENTRYPOINT y agregamos:

```
ENTRYPOINT ["/usr/sbin/nginx", "-g", "daemon off;"]
```

Este ENTRYPOINT ejecuta nginx como foreground dentro del contenedor

```
$ docker build -t demo-nginx:1.0 .
```

Valide la imagen nueva con "docker images", y ejecute un nuevo contenedor con nombre "demo-nginx":

```
docker run --name demo-nginx -p 80:80 -d demo-nginx:1.0
```

El parametro "-p" redirige el trafico que viene del puerto 80 hacia el 80 del contenedor. El parametros "-d" (detach) sirve para que la terminal no quede capturada por la ejecución del contenedor

validamos:

```
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                NAMES
1375502d8961        demo-nginx:1.0      "/usr/sbin/nginx -g …"   3 seconds ago       Up 1 second         0.0.0.0:80->80/tcp   demo-nginx
```

Listo, tenemos nuestro nginx instalado en la imagen de ubuntu, probemos el acceso desde fuera:

```
curl http://127.0.0.1
```

la salida:

```
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

Genial, ya tenemos nuestro container con nginx corriendo.

## Otras operaciones

Antes de finalizar este punto, es posible realizar otras operaciones con el container corriendo:

* Copiar archivo hacia el contenedor. 

Cree un archivo con "touch archivo.txt" y copielo "docker cp archivo.txt [container_id/name]":

```
$ docker cp archivo.txt demo-nginx:tmp
```

Para validar que el archivo exista, ejecute "docker exec" para ingresar al contenedor y utilizar una shell:

```
$ docker exec -ti demo-nginx /bin/bash
ls -l /tmp/
```

El archivo debe estar en el contenedor. Salga del contenedor con "exit".


## 7. Volumenes de datos

Cuando un contenedor es borrado, toda la información que tiene el contenedor desaparece. Para que el almacenamiento sea persistente en el contenedor es necesario utilizar volúmenes de datos (data volume). El volumen es un directorio o archivo que se monta en el contenedor. Se pueden montar varios volumenes en un contenedor y varios contenedores pueden montar un mismo volumen.

Los volúmenes de datos tienen las siguientes características:

* Son utilizados para guardar e intercambiar información de forma independientemente a la vida de un contenedor.
* Nos permiten guardar e intercambiar información entre contenedores.
* Cuando borramos el contenedor, no se elimina el volumen asociado.
* Los volúmenes de datos son directorios del host montados en un directorio del contenedor, aunque también se pueden montar ficheros.
* En el caso de montar en un directorio ya existente de un contenedor un volumen de datos , su contenido no será eliminado.

### Añadiendo un volumen de datos

Un ejemplo para añadir un volumen:

```
docker run -it --name contenedor1 -v /volumen ubuntu:latest bash
```

El comando correra un nuevo contenedor y creara un volumen de nombre "volumen", con imagen ubuntu y con shell "bash". Ya dentro del contenedor revisamos:

```
# cd /volumen/
# touch filetest.txt
# exit
```

Para validar el nuevo volumen:

```
$ docker inspect contenedor1
```

La salida es muy amplia, pero nos enfocaremos por ahora en "Mounts":

```
$ docker inspect contenedor1|grep -A 10 "Mounts"
        "Mounts": [
            {
                "Type": "volume",
                "Name": "a22d199fd9a5cc0d87a6175455a6285188b160ceeed64e04380ad53069cd633e",
                "Source": "/var/lib/docker/volumes/a22d199fd9a5cc0d87a6175455a6285188b160ceeed64e04380ad53069cd633e/_data",
                "Destination": "/volumen",
                "Driver": "local",
                "Mode": "",
                "RW": true,
                "Propagation": ""
            }
```

Se puede encontra datos como tipo de volumen, nombre de volumen, origen (directorio en servidor local), Destino en estructura de directorio en el contenedor, entre otros.

Puede validar que el archivo creado en el volumen existe:

```
$ sudo su -
# ls -l /var/lib/docker/volumes/a22d199fd9a5cc0d87a6175455a6285188b160ceeed64e04380ad53069cd633e/_data
total 0
-rw-r--r-- 1 root root 0 Jun 11 15:51 filetest.txt
# exit
```

Tambien es posible revisar todo los volumenes creado con:

```
$ docker volume ls
DRIVER              VOLUME NAME
local               a22d199fd9a5cc0d87a6175455a6285188b160ceeed64e04380ad53069cd633e
```

Si borramos el contenedor, el volumen permanece:

```
$ docker rm contenedor1
contenedor1
$ docker volume ls
DRIVER              VOLUME NAME
local               a22d199fd9a5cc0d87a6175455a6285188b160ceeed64e04380ad53069cd633e
```

## Creando un volumen de datos

Anteriormente se agrego un volumen al contenedor con un nombre complicado. Para facilitar podemos crear el volumen previamente y asignar un nombre:

```
$ docker volume create --name vol1
vol1
```

```
$ docker volume ls
DRIVER              VOLUME NAME
local               a22d199fd9a5cc0d87a6175455a6285188b160ceeed64e04380ad53069cd633e
local               vol1
```

Ahora asociamos el contenedor:

```
$ docker run -it --name contenedor2 -v vol1:/data ubuntu:latest bash
# cd data
# exit
```

El volumen "vol1" se monta en "/data" del contenedor. 

Es posible realizar lo anterior sin crear el volumen manualmente agregando el nombre del volumen a crear:

```
$ docker run -it --name contenedor3 -v vol2:/data ubuntu:latest bash
root@110c30c36baf:/# ls -l /data
total 0
root@110c30c36baf:/# exit
```

Y validar el volumen:

```
$ docker volume ls
DRIVER              VOLUME NAME
local               a22d199fd9a5cc0d87a6175455a6285188b160ceeed64e04380ad53069cd633e
local               vol1
local               vol2
```

Tambien podemos asociar el contenedor con el nombre lago del volumen que creamos inicialmente:

```
$ docker run -it --name contenedor4 -v a22d199fd9a5cc0d87a6175455a6285188b160ceeed64e04380ad53069cd633e:/data ubuntu:latest bash
root@9466ba10ff56:/# ls -l /data/
total 0
-rw-r--r-- 1 root root 0 Jun 11 15:51 filetest.txt
root@9466ba10ff56:/# exit
```

Por último para borrar un volumen tenemos que asegurarnos que no está asociado a ningún contenedor:

```
$ docker volume rm vol1
Error response from daemon: remove vol1: volume is in use - [f3db9bcbce627cc36979b68b6001fbe5b5afcc39093c388aed2cf8703eeb1222]
bt@brbktdevopstest:~/docker$ docker ps -a
CONTAINER ID        IMAGE               COMMAND                   CREATED             STATUS                      PORTS                NAMES
9466ba10ff56        ubuntu:latest       "bash"                    22 minutes ago      Exited (0) 4 minutes ago                         contenedor4
110c30c36baf        ubuntu:latest       "bash"                    25 minutes ago      Exited (0) 24 minutes ago                        contenedor3
f3db9bcbce62        ubuntu:latest       "bash"                    30 minutes ago      Exited (0) 25 minutes ago                        contenedor2
1375502d8961        demo-nginx:1.0      "/usr/sbin/nginx -g …"    3 hours ago         Up 3 hours                  0.0.0.0:80->80/tcp   demo-nginx
7549cd55116b        demo-nginx:1.0      "/usr/sbin/nginx -g …"    2 days ago          Exited (0) 2 days ago                            brave_proskuriakova
ba5d1e1eed26        demo-ubuntu:1.0     "/bin/sh -c 'echo \"E…"   3 days ago          Exited (0) 3 days ago                            fervent_wescoff
```
Segun el id "f3db9bcbce627cc36979b68b6001fbe5b5afcc39093c388aed2cf8703eeb1222" corresponde al "contenedor2", borrelo usando el "CONTAINER_ID" o "NAMES":

```
$ docker rm f3db9bcbce627cc36979b68b6001fbe5b5afcc39093c388aed2cf8703eeb1222
f3db9bcbce627cc36979b68b6001fbe5b5afcc39093c388aed2cf8703eeb1222
$ docker volume rm vol1
vol1
```

Listo.

```
$ docker volume ls
DRIVER              VOLUME NAME
local               a22d199fd9a5cc0d87a6175455a6285188b160ceeed64e04380ad53069cd633e
local               vol2
```

## Contenedores de volúmenes de datos

Otra posibilidad es crear un contenedor con un volumen de datos y asociarlo a multiples contenedores.

* Crear un contenedor y asigna un volumen

```
$ docker create -it --name data_volumen_container -v /shared_folder ubuntu:latest bash
```

* A continuación con el parámetro --volumes-from, creamos un contenedor asociado al contenedor anterior y que montará el volumen de datos:

```
$ docker run -it --name container1 --volumes-from data_volumen_container ubuntu:latest bash
# cd shared_folder/
# ls -l
total 0
# echo "hola">fichero.txt
# ls -l
total 4
-rw-r--r-- 1 root root 5 Jun 11 19:15 fichero.txt
# exit
```

Finalmente creamos un nuevo contenedor asociado al contenedor con el volumen de datos, para comprobar que, efectivamente, se comparte el volumen:

```
$ docker run -it --name container2 --volumes-from data_volumen_container ubuntu:latest bash
root@1843604aa98c:/# cd shared_folder/
root@1843604aa98c:/shared_folder# ls -l
total 4
-rw-r--r-- 1 root root 5 Jun 11 19:15 fichero.txt
root@1843604aa98c:/shared_folder# 
```

## 8. Redes en docker

Internamente docker maneja un red propia para correr los contenedores. Al momento de instalar e iniciar docker creara una red interna que puede ser validada:

```
$ sudo ip a
```

la salida:

```
6: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    link/ether 02:42:6b:04:34:1a brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:6bff:fe04:341a/64 scope link 
       valid_lft forever preferred_lft forever
```
La red por defecto que maneja docker es 172.17.0.0/16 (bridge). Esta red puede ser modificada, pero para efectos del curso no tocaremos esta configuración.

* Redes que maneja docker:

Las redes que maneja docker son:

```
$ docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
c35d5364bb46        bridge              bridge              local
6fab55924e5d        host                host                local
ac12516aaa8d        none                null                local
```

* Bridge = Maneja la red 172.17.0.0/16 para los contenedores.
* host = Este controlador elimina el aislamiento entre los contenedores y el anfitrión.
* none = Este modo no configurará ninguna IP para el contenedor y no tiene acceso a la red externa ni a otros contenedores. Tiene la dirección de looplocal.

Para mejor entendimiento, haremos un ejercicio rapido de las redes.

Ejecute 2 contenedores con la imagen alpine (imagen muy ligera)

```
$ docker run -dit --rm --name alpine1 alpine sh
$ docker run -dit --rm --name alpine2 alpine sh
```

```
$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
59aec6c89580        alpine              "sh"                3 seconds ago       Up 2 seconds                            alpine2
d06a6456212a        alpine              "sh"                10 seconds ago      Up 9 seconds                            alpine1
```

El parametros "-dit" levanta el contenedor de forma interactiva y con detach, y "--rm" al momento de detener el contenedor, tambien lo borrara.

Ahora para validar las IP's de los contenedores:

```
$ docker network inspect bridge
[
    {
        "Name": "bridge",
        "Id": "c35d5364bb46153888b27b61bc77473eaff0bba458b1d70ea027308ca439925d",
        "Created": "2020-06-16T08:30:24.111133123-04:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.17.0.0/16",
                    "Gateway": "172.17.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "59aec6c89580451de2d55def08bec15acb271d284033c963b2db8c12bcbf85c4": {
                "Name": "alpine2",
                "EndpointID": "192f1f15d5c52b8361142b2f0ff4b35dc2b0f3182524c16a62e67c2f8203effb",
                "MacAddress": "02:42:ac:11:00:03",
                "IPv4Address": "172.17.0.3/16",
                "IPv6Address": ""
            },
            "d06a6456212a184e4a7b3af4d43f730317bac06e0a6629565ff4321a13df69c6": {
                "Name": "alpine1",
                "EndpointID": "e20c864e038ee3e131ea39c3654a3c532c7677b2a91f1eb34cab9b95dae1fd13",
                "MacAddress": "02:42:ac:11:00:02",
                "IPv4Address": "172.17.0.2/16",
                "IPv6Address": ""
            }
        },
        "Options": {
            "com.docker.network.bridge.default_bridge": "true",
            "com.docker.network.bridge.enable_icc": "true",
            "com.docker.network.bridge.enable_ip_masquerade": "true",
            "com.docker.network.bridge.host_binding_ipv4": "0.0.0.0",
            "com.docker.network.bridge.name": "docker0",
            "com.docker.network.driver.mtu": "1500"
        },
        "Labels": {}
    }
]
```
La salida muestra que el contenedor "alpine1" tiene ip 172.17.0.2/16 y "alpine2" con ip 172.17.0.3/16. Si ejecuta un ping desde el servidor anfitrión las ip 172.17.0.2 y 172.17.0.3 responderan sin problemas, pero si ejecuta un ping a "alpine1" o "alpine2" no resolvera el nonbre.

Ahora, ingrese a el contenedor "alpine1"

```
$ docker attach alpine1
```

Si hace un ping a la ip de alpine2 con ip 172.17.0.3:

```
$ docker attach alpine1
/ # ping 172.17.0.3
PING 172.17.0.3 (172.17.0.3): 56 data bytes
64 bytes from 172.17.0.3: seq=0 ttl=64 time=0.150 ms
^C
--- 172.17.0.3 ping statistics ---
1 packets transmitted, 1 packets received, 0% packet loss
round-trip min/avg/max = 0.150/0.150/0.150 ms
/ # 
```

Para salir del contenedor sin detenerlo, ejecuta "ctrl+p ctrl+q".

Ahora, detenga el contenedor "alpine2".

```
$ docker stop alpine2
alpine2
```

Luego, ejecute un nuevo contenedor con "--link".

```
$ docker run -dit --rm --name alpine2 --link alpine1 alpine sh
a6ce054c20f4695f462e2213acc01ad0a549fde0a6ac56290a9cc9408cedf7a0
$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
a6ce054c20f4        alpine              "sh"                3 seconds ago       Up 2 seconds                            alpine2
d06a6456212a        alpine              "sh"                34 minutes ago      Up 34 minutes                           alpine1
```

alpine2 fue ejecutado con un enlace a alpine1, entonces entramos a "alpine2" y hacemos ping a "alpine1":

```
$ docker attach alpine2
/ # ping alpine1
PING alpine1 (172.17.0.2): 56 data bytes
64 bytes from 172.17.0.2: seq=0 ttl=64 time=0.231 ms
64 bytes from 172.17.0.2: seq=1 ttl=64 time=0.153 ms
^C
--- alpine1 ping statistics ---
2 packets transmitted, 2 packets received, 0% packet loss
round-trip min/avg/max = 0.153/0.192/0.231 ms
/ # 
```

Ahora si tenemos ping a traves del nombre!. Ahora detenga los contenedores:

```
$ docker stop alpine1 alpine2
```
 
## Creando nuevas redes

Ahora, crearemos una nueva red para nuestros contenedores. 

```
$ docker network create --driver bridge red1
b2524b88d6efd2bbf665a525ead83d80220f21a5cee51188086201a288b98e73
$ docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
c35d5364bb46        bridge              bridge              local
6fab55924e5d        host                host                local
ac12516aaa8d        none                null                local
b2524b88d6ef        red1                bridge              local
```

Cree 2 contenedores utilizando la nueva "red1":

```
$ docker run -dit --rm --name alpine1 --network red1 alpine sh
b1c8efe3aa47973b86236d9e7d686b7e0ce81026f14f17ef8d55563fa37967d0
$ docker run -dit --rm --name alpine2 --network red1 alpine sh
1a5f10adb342a74d70d6b8fa8997bc567d3e2a04ff1f0574535d5dd711ce41c0
```

Los contenedores fueron creados con la red1.

```
$ docker network inspect red1
[
    {
        "Name": "red1",
        "Id": "b2524b88d6efd2bbf665a525ead83d80220f21a5cee51188086201a288b98e73",
        "Created": "2020-06-16T19:20:13.757023818-04:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.18.0.0/16",
                    "Gateway": "172.18.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "1a5f10adb342a74d70d6b8fa8997bc567d3e2a04ff1f0574535d5dd711ce41c0": {
                "Name": "alpine2",
                "EndpointID": "c1a629eb7cdb56c76863695ee8f91f51b5f476b9f59a1c22ab2a81a339b97bbc",
                "MacAddress": "02:42:ac:12:00:03",
                "IPv4Address": "172.18.0.3/16",
                "IPv6Address": ""
            },
            "b1c8efe3aa47973b86236d9e7d686b7e0ce81026f14f17ef8d55563fa37967d0": {
                "Name": "alpine1",
                "EndpointID": "693f685734f6c05d59bbcefa8e9e66279122e50182c06eaf986e5e73b52f92e4",
                "MacAddress": "02:42:ac:12:00:02",
                "IPv4Address": "172.18.0.2/16",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {}
    }
]
```

Listo, ingrese a los contenedores y realice pruebas de ping entre alpine2 y alpine2.

## Un contenedor conectado a 2 redes.

Es posible tener un contenedor conectado a dos redes Docker. Para ello creamos un nueva red docker:

```
$ docker network create --driver bridge red2
```

Ahora utilizamos la nueva red para crear un contenedor:

```
$ docker run -dit --rm --name alpine3 --network red2 alpine sh
024ade2dc4cbb45ac5b15ddb8ec5063aa3d23b7def8792e5a5d1a144c954291a
```

Una vez creado el contenedor, conectaremos el contenedor "alpine3" en la red1. 

```
$ docker network connect red1 alpine3
```
Entonces "alpine3" estará conectado en red2 y red1. Puede ingresar al contenedor "alpine3" y hacer ping a contenedores en distintas redes.

```
$ docker attach alpine3
/ # ping alpine2
PING alpine2 (172.18.0.3): 56 data bytes
64 bytes from 172.18.0.3: seq=0 ttl=64 time=0.271 ms
^C
--- alpine2 ping statistics ---
1 packets transmitted, 1 packets received, 0% packet loss
round-trip min/avg/max = 0.271/0.271/0.271 ms
/ # ping alpine1
PING alpine1 (172.18.0.2): 56 data bytes
64 bytes from 172.18.0.2: seq=0 ttl=64 time=0.188 ms
^C
--- alpine1 ping statistics ---
1 packets transmitted, 1 packets received, 0% packet loss
round-trip min/avg/max = 0.188/0.188/0.188 ms
/ # 
```

Por ultimo, detenga todos los contenedores alpine:

```
$ docker stop alpine1 alpine2 alpine3
alpine1
alpine2
alpine3
```

Elimine las redes que no tiene contenedores corriendo

```
$ docker network prune
WARNING! This will remove all custom networks not used by at least one container.
Are you sure you want to continue? [y/N] Y
Deleted Networks:
red1
red2
```

Valide que red1 y red2 no existan.
```
$ docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
c35d5364bb46        bridge              bridge              local
6fab55924e5d        host                host                local
ac12516aaa8d        none                null                local
```

## 9. Publicando imagen en https://hub.docker.com/

Para publicar una imagen en Hub docker, necesita una cuenta y crear un repositorio:

![Screenshot](/images/hub_docker.jpg)

Debe logearse en hub docker:

```
docker login
```

Ahora es necesario asignar un tag a la imagen que queremos subir:

```
docker tag demo-nginx:1.0 fregular01/devops_test:demo-nginxV1
```

Ahora publicamos la imagen "demo-nginx":

```
$ docker push fregular01/devops_test:demo-nginxV1
```

Valide que su imagen esta en su repositorio:

![Screenshot](/images/hub_docker02.jpg)

Puede validar la imagen remota:

```
$ docker images
REPOSITORY               TAG                 IMAGE ID            CREATED             SIZE
fregular01/devops_test   demo-nginxV1        56856ce92109        3 days ago          155MB
demo-nginx               1.0                 56856ce92109        3 days ago          155MB
demo-ubuntu              1.0                 4fdca8fe208a        3 days ago          155MB
ubuntu                   latest              1d622ef86b13        6 weeks ago         73.9MB
hello-world              latest              bf756fb1ae65        5 months ago        13.3kB
```


## 10. Docker-compose

Compose es una herramienta para definir y ejecutar aplicaciones Docker de contenedores múltiples. Con Compose, utiliza un archivo YAML para configurar los servicios de su aplicación. Luego, con un solo comando, crea e inicia todos los servicios desde su configuración. 

Para evitar conflictos posteriores, detenga el contenedor "demo-nginx":

```
$ docker stop demo-nginx
```

Antes de comenzar, primero hay que instalar:

```
$ sudo apt-get install docker-compose -y
```

Una vez instalado, utilizaremos un ejemplo de que existe en el gitlab del curso docker. Clone el projecto:

```
$ git clone https://gitlab.com/gole-group/devops-base-01/docker.git
```

una vez clonado, ingresa a "cd flask/" y tendra un listado de archivo, entre ellos el Dockerfile y docker-compose.yml. Analizaremos el Dockerfile:

```
FROM python:3.7-alpine
WORKDIR /code
ENV FLASK_APP flask_redis.py
ENV FLASK_RUN_HOST 0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
CMD ["flask", "run"]
```

El Dockerfile :

* Utiliza una imagem Python 3.7.
* Establezce el directorio de trabajo en / code.
* Establezce las variables de entorno utilizadas por el flask command.
* Instala gcc para que los paquetes de Python como MarkupSafe y SQLAlchemy puedan compilar aceleraciones.
* Copia requiremets.txt e instale las dependencias de Python.
* Copia el directorio actual "." en el proyecto al workdir "." de la imagen.
* Establezce el comando predeterminado para que el contenedor ejecute flask.

El docker-compose.yaml

```
version: '3'
services:
  web:
    build: .
    ports:
      - "80:5000"
  redis:
    image: "redis:alpine"
```

El compose  Utiliza 2 service:
* Web service que utiliza flask y redirecciona el puerto 80 hacia el puerto 5000 nativo de flask.
* Extrae una imagen publica de redis de hub docker

El flask_redis.py inicia una API en flask basica con la función de insertar variables y extraer de Redis (motor de base de datos en memoria).

Ya tenemos todo listo, ejecutamos el docker-compose:

```
$ docker-compose up
```

Compose trae una imagen de Redis, crea una imagen para su código e inicia los servicios que definió. En este caso, el código se copia estáticamente en la imagen en el momento de la compilación. El terminal quedará tomado por flask. Habrá otra terminal y ejecute el script python para insertar un key/value en Redis a traves de la API flask:

```
$ key=name value=john python3 set_key.py
```

Listo, puede validar la inserción de "key/value" ingresando al contenedor:

```
$ docker exec -ti flask_redis_1 sh
/data # redis-cli
127.0.0.1:6379> keys *
1) "name"
127.0.0.1:6379> get name
"john"
127.0.0.1:6379> exit
/data # exit
```

Para extraer el "value" de un "key" ejecute el script:

```
$ key=name python3 get_key.py
john
```

Por ultimo, para ejecutar el docker-compose como demonio, agregue el parametros "-d":

```
docker-compose up -d
```

## 10. logs de contenedores

Si la aplicación maneja logs, puede visualizarlos ejecutando "docker logs":

```
$ docker logs flask_redis_1
1:C 13 Jun 2020 04:56:55.654 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
1:C 13 Jun 2020 04:56:55.656 # Redis version=6.0.5, bits=64, commit=00000000, modified=0, pid=1, just started
1:C 13 Jun 2020 04:56:55.656 # Warning: no config file specified, using the default config. In order to specify a config file use redis-server /path/to/redis.conf
1:M 13 Jun 2020 04:56:55.660 * Running mode=standalone, port=6379.
1:M 13 Jun 2020 04:56:55.660 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
1:M 13 Jun 2020 04:56:55.660 # Server initialized
```

