---
title: "Chef attributes"
date: 2021-04-20T15:30:28-04:00
draft: false
---

Continuando con la serie de chef, seguimos con los atributos. Un atributo no es más que un par clave-valor que representa un detalle específico sobre un nodo. El chef-client (mas adelante se explicará como utilizarlo) utiliza los atributos para comprender el estado actual del nodo. Cuál era el estado del nodo al final de la ejecución anterior de chef-cliente. 

Los atributos los podemos llamar desde ```attributes/default.rb```. Es util cuando necesitamos definir valores que puede ser reutilizados en multiples nodos, asi evitar agregar valores en duro.

# 1. Prerrequisitos

Se reutilizara el repositorio de chef utilizado en [chef template](https://fregular.gitlab.io/portafolio/publicaciones/chef_templates/).

# 2. Comenzamos

Primero debemos crear la carpeta "attributes/". Para ello ejecutamos:

```
$ chef generate attribute  default
```

Esto generará la carpeta "attributes/default.rb", este archivo podemos agregar los atributos que necesitemos.

Edite el "attributes/default.rb", y agregue esto:

```
default['base']['message'] = "Mensaje de ejemplo\n"
```

Ahora edite el "recipes/default.rb".

```
$ cat recipes/default.rb
#
# Cookbook:: web
# Recipe:: default
#
# Copyright:: 2021, The Authors, All Rights Reserved.
apt_update

package 'apache2'

template '/var/www/html/index.html' do
  source 'index.html.erb'
end

service 'apache2' do
  action [:enable, :start]
end


file '/etc/motd' do
  content node['base']['message']
end
```

El contenido de "/etc/motd" sera el especificado "Mensaje de ejemplo" que viene de los atributos. Ahora solo queda aplicar el cambio:

```
$ kitchen converge ubuntu
```

Ahora solo queda logearse.

```
$ kitchen login ubuntu
Welcome to Ubuntu 20.04.1 LTS (GNU/Linux 5.4.0-58-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Wed 21 Apr 2021 04:58:19 AM UTC

  System load:  0.17              Processes:             106
  Usage of /:   2.6% of 61.31GB   Users logged in:       0
  Memory usage: 18%               IPv4 address for eth0: 10.0.2.15
  Swap usage:   0%


This system is built by the Bento project by Chef Software
More information can be found at https://github.com/chef/bento
Mensaje de ejemplo
Last login: Wed Apr 21 04:57:54 2021 from 10.0.2.2
vagrant@default-ubuntu-2004:~$
```

Listo, el mensaje aparece sin problemas. Los atributos son utiles para definir valores que se reutilizan, y aplicarlos en recetas, por ejemplo aplicar configuraciones, crear usuario con parametros llamados desde los atributos.


