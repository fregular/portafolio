---
title: "Chef infra introducción"
date: 2021-04-19T10:39:20-04:00
draft: false
---
## Introducción a Chef.io

Chef  es un herramienta de automatización que tiene como fin convertir nuestra infraestructura en código. Esta herramienta es muy utilizada
dentro de la cultura Devops. Dentro de sus ventajas es que puede ser utilizado tanto en infraestructuras on-premise como en la nube. 

Chef para administrar su flota (nodos cliente) necesita de un agente en los nodos, para luego sincronizar la configuración establecida en un cookbook. 

Para iniciar en lo mas básico de chef, se explicara Chef Infra y se realizara prueba locales. Mas adelante se creara un laboratorio iniciar para probar otras funcionalidades



# 1. Prerrequisitos

Para comenzar a jugar, se utilizara todos los componentes en ubuntu 20.04. Para empezar se necesita:

* [Chef Workstation](https://downloads.chef.io/tools/workstation)
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant](https://www.vagrantup.com/downloads)

Una vez instalados, valide que este todo:

```
$ chef --version
```

```
$ VBoxManage --version
```

```
$ vagrant --version
```

Si los comandos se ejecutan sin problema, podemos iniciar con el laboratorio.


# 2. Comenzamos!!

Para comenzar, cree un directorio

```
$ mkdir ~/learn-chef-infra && cd ~/learn-chef-infra
```

Ya dentro de la carpeta, genere un cookbook base para aprender:

```
$ chef generate cookbook learn_chef
```

Ingrese al directorio

```
$ cd learn_chef
```

Dentro del directorio tenemos el kitchen.yml

```
cat kitchen.yml
---
driver:
  name: vagrant

## The forwarded_port port feature lets you connect to ports on the VM guest via
## localhost on the host.
## see also: https://www.vagrantup.com/docs/networking/forwarded_ports

#  network:
#    - ["forwarded_port", {guest: 80, host: 8080}]

provisioner:
  name: chef_zero

  ## product_name and product_version specifies a specific Chef product and version to install.
  ## see the Chef documentation for more details: https://docs.chef.io/workstation/config_yml_kitchen/
  #  product_name: chef
  #  product_version: 16

verifier:
  name: inspec

platforms:
  - name: ubuntu-20.04
  - name: centos-8

suites:
  - name: default
    verifier:
      inspec_tests:
        - test/integration/default
    attributes:
```

Este archivo se utiliza para hacer prueba locales. El comando Kitchen utiliza este archivo para generar las maquinas virtuales para probar los cookbook. El driver que se utiliza
es vagrant y este se comunica con virtualbox para generar las maquinas. En platforms esta ubuntu-20.04 y centos-8 (al momento de hacer pruebas tuve problemas, recomiendo cambiar a centos-7 solo con el fin de que el laboratorio funcione). También esta la parte de Suites que se utiliza para hacer test unitarios de nuestros cookbooks.

Si ejecutamos:

```
$ kitchen list
Instance             Driver   Provisioner  Verifier  Transport  Last Action    Last Error
default-ubuntu-2004  Vagrant  ChefZero     Inspec    Ssh        <Not Created>  <None>
default-centos-8     Vagrant  ChefZero     Inspec    Ssh        <Not Created>  <None>
```

Esta la lista de instancia que podemos utilizar para probar nuestros cookbooks y recetas.

Para iniciar las instancias ejecute:

```
$ kitchen create
```

Esto generará ambas instancias. Puede validar las instancias ejecutando la gráfica de virtualbox.

Para ingresar a una instancia, ejecute:

```
$ kitchen login ubuntu
```

Si quiere ingresar a centos, cambie ubuntu por centos.


# 3. escribiendo codigo de infra

Ahora la parte de mas entretenida, crear recetas en nuestro cookbook. Las recetar van en el directorio ```recipes/```. Dentro de este recipes cree este archivo:

```
$ cat learn-chef.yml
resources:
 - type: "file"
   name: "/etc/motd"
   content: "Learning Chef is fun with YAML!"
```

Esta receta esta escrito en formato yml, utiliza un recurso del tipo "file", en el archivo /etc/motd con el contenido "Learning Chef is fun with YAML!". Ahora que la receta esta lista, se debe agregar en default.rb, esta receta esta escriba en ruby y para ejecutar las pruebas con kitchen este mira por defecto default.rb. Para incluir la receta ```learn-chef.yml``` en default.rb edítelo:

```
$ cat recipes/default.rb
#
# Cookbook:: learn_chef
# Recipe:: default
#
# Copyright:: 2021, The Authors, All Rights Reserved.
include_recipe "learn_chef::learn-chef"

```

El *include_recipe* se agrega las recetas que queremos agregar. El formato es ```cookbook::receta```.

Ahora queda deployar los cambio con el comando:

```
$ kitchen converge
```

Una vez los cambios estén aplicados, ingrese al servidor:

```
$ kitchen login ubuntu
Welcome to Ubuntu 20.04.1 LTS (GNU/Linux 5.4.0-58-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Mon 19 Apr 2021 07:05:33 PM UTC

  System load:  0.01              Processes:             105
  Usage of /:   2.6% of 61.31GB   Users logged in:       0
  Memory usage: 18%               IPv4 address for eth0: 10.0.2.15
  Swap usage:   0%


This system is built by the Bento project by Chef Software
More information can be found at https://github.com/chef/bento
Learning Chef is fun with YAML!
Last login: Mon Apr 19 19:00:39 2021 from 10.0.2.2

```

El mensaje del día aparece cuando ingresamos.

# 4. Configurando un web server

Ahora se agrega otra receta para instalar un webserver y  subir un contenido de prueba.

cree un archivo web.yml en ```recipe/```, con este contenido:

```
resources:
 - type: "package"
   name: "apache2"
 - type: "file"
   name: "/var/www/html/index.html"
   content: ""
 - type: "service"
   name: "apache2"
   action:
    - enable
    - start
```

Este receta utiliza el recurso de tipo package para instalar apache2, también utiliza el recurso file para crear un index, con un contenido vacios (puede modificarlos si quiere), y el recurso service para habilitar e iniciar apache2.

Ahora agregue esta receta a ```recipes/default.rb```:

```
$ cat recipes/default.rb
#
# Cookbook:: learn_chef
# Recipe:: default
#
# Copyright:: 2021, The Authors, All Rights Reserved.
include_recipe "learn_chef::learn-chef"
include_recipe "learn_chef::web"
```

y deploye el cambio:

```
$ kitchen converge ubuntu
```

Ingrese a ubuntu:

```
$ kitchen login ubuntu
```

ejecute un curl para validar.

```
$ curl localhost
```

También puede validar si apache esta instalado y habilitado. 

Salga del servidor y aplique ```kitchen destroy ubuntu``` para eliminar la maquina.

Las siguientes publicaciones se hablara sobre:
* Templates
* Attributes
* Policyfiles
* Unit Test
* Databag
* Instalación de chef server:
    * instalación de agent
    * subiendo cookbooks a chef server
    * corriendo cookbook en los nodos
    * Roles
