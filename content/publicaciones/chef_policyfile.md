---
title: "Chef policyfile"
date: 2021-04-21T09:58:16-04:00
draft: false
---

# Policyfile en Chef

Ahora toca hablar un poco de Policyfile, este archivo de politicas contiene un contenido asi:

```
# Policyfile.rb - Describe how you want Chef Infra Client to build your system.
#
# For more information on the Policyfile feature, visit
# https://docs.chef.io/policyfile/

# A name that describes what the system you're building with Chef does.
name 'web'

# Where to find external cookbooks:
default_source :supermarket

# run_list: chef-client will run these recipes in the order specified.
run_list 'web::default'

# Specify a custom source for a single cookbook:
cookbook 'web', path: '.'
```

En el define:
* Nombre de cookbook
* Donde buscará los cookbook (en este caso es el supermarket, que es un repositorio externo de cookbook que son de libre uso)
* run_list define el cookbook y las recetas que se ejecutaran, por defecto ejecutara el default, pero puede modificarse o agregar otros cookbook
* Tambien se puede especificar otros cookbook que estan en otras rutas y que pueden ser utilizados por este playbook

Ahora para ejemplificar esto, necesitamos instalar los prerrequisitos

# 1. Prerrequisitos

Para comenzar a jugar, se utilizara todos los componentes en ubuntu 20.04. Para empezar se necesita:

* [Chef Workstation](https://downloads.chef.io/tools/workstation)
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant](https://www.vagrantup.com/downloads)

Una vez instalados, valide que este todo:

```
$ chef --version
```

```
$ VBoxManage --version
```

```
$ vagrant --version
```

Si los comandos se ejecutan sin problema, podemos iniciar con el laboratorio.


# 2. Comenzamos!!

Primero necesita clonar este proyecto:

```
$ git clone https://github.com/learn-chef/lcr-policyfiles-getstarted.git
$ cd lcr-policyfiles-getstarted
```

Dentro del proyecto se encuentra esta estructura de directorio:

```
$ tree -L 2
.
├── LICENSE
├── README.md
├── chefignore
└── cookbooks
    ├── README.md
    ├── hardening
    └── patching

3 directories, 4 files
```

Se tiene 2 cookbooks, el hardening y el patching. Estos cookbooks seran utilizados como dependencias de un nuevo cookbook para visualizar con mayor detalle la funcionalidad que tiene Policyfile.

Ambos cookbook viene del supermarket que puedes encontrar aca:


https://supermarket.chef.io/cookbooks/os-hardening
https://supermarket.chef.io/cookbooks/windows-hardening


# 3. Creando el cookbooks wrapper

Crea un nuevo cookbook:

```
$ chef generate cookbook cookbooks/base -P
```

**nota: El "-P" asegura que dentro del cookbook "base" tendra un policyfile propio.**


Ahora edite el recipe "default.rb" del nuevo cookbooks con el siguiente contenido (/base/recipes/default.rb):


```
#
# Cookbook:: base
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

include_recipe 'hardening::default'

file '/etc/motd' do
  content node['base']['message']
end
```

El recipe llama a un attributo ['base']['message'], por lo tanto lo creamos:

```
$ chef generate attribute cookbooks/base default
```

edite cookbooks/base/attributes/default.rb

```
default['base']['message'] = "This node was hardened by Chef\n"
```

Lo siguiente es definir en el archivo metadata.rb del cookbook base, la dependencia de cookbook 'hardening'.

```
name 'base'
maintainer 'The Authors'
maintainer_email 'you@example.com'
license 'All Rights Reserved'
description 'Installs/Configures base'
version '0.1.0'
chef_version '>= 15.0'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
# issues_url 'https://github.com/<insert_org_here>/base/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
# source_url 'https://github.com/<insert_org_here>/base'
depends 'hardening'

```

# 4. Policyfile

El Policyfile por defecto es asi:

```
$ cat Policyfile.rb
# Policyfile.rb - Describe how you want Chef Infra Client to build your system.
#
# For more information on the Policyfile feature, visit
# https://docs.chef.io/policyfile/

# A name that describes what the system you're building with Chef does.
name 'base'

# Where to find external cookbooks:
default_source :supermarket

# run_list: chef-client will run these recipes in the order specified.
run_list 'base::default'

# Specify a custom source for a single cookbook:
cookbook 'base', path: '.'
cookbook 'hardening', path: '../hardening'

# Policyfile defined attributes
default['base']['message'] = "This node was hardened by Chef. Policyfile created at #{Time.now.utc}\n"
```

Al final del archivo agregue este contenido:

```
cookbook 'hardening', path: '../hardening'
```

Esta linea le indica una ruta alternativa para otros cookbook, en este caso el de hardening.


# 5. Instalación del Policyfile

Una vez modificado el Policyfile, se debe instalar estos cambios.

```
$ chef install Policyfile.rb
Building policy base
Expanded run list: recipe[base::default]
Caching Cookbooks...
Installing base      >= 0.0.0 from path
Installing hardening >= 0.0.0 from path
Using      os-hardening            4.0.0
Using      windows-hardening       0.9.1
Using      windows-security-policy 0.3.9

Lockfile written to /home/fregular/projectos_regu/chef/learning-chef-infra/lcr-policyfiles-getstarted/cookbooks/base/Policyfile.lock.json
Policy revision id: 74bc858753d703a11ab9477384d316b14838afb821f355257d42a8272cf00ee2
```

Este comando genero un archivo "Policyfile.lock.json" con la configuración del policyfile, la huella SHA, un checksum etc.

Ahora solo queda aplicar la convergencia, para ello verifique que kitchen.yml tenga este contenido:

```
---
driver:
  name: vagrant

## The forwarded_port port feature lets you connect to ports on the VM guest via
## localhost on the host.
## see also: https://www.vagrantup.com/docs/networking/forwarded_ports

#  network:
#    - ["forwarded_port", {guest: 80, host: 8080}]

provisioner:
  name: chef_zero

  ## product_name and product_version specifies a specific Chef product and version to install.
  ## see the Chef documentation for more details: https://docs.chef.io/workstation/config_yml_kitchen/
  #  product_name: chef
  #  product_version: 16

verifier:
  name: inspec

platforms:
  - name: ubuntu-20.04
  - name: centos-7

suites:
  - name: default
    verifier:
      inspec_tests:
        - test/integration/default
    attributes:
```

Ahora aplique la convergencia al centos-7:

```
$ kitchen converge centos
```

Ahora valide que el mensaje del dia sea el especificado en los atributos:

```
$ kitchen login centos
Last login: Thu Feb  7 14:22:02 2019 from 10.0.2.2
This node is hardened by Chef
[vagrant@default-centos-7 ~]$
```

Bien, una vez validado, salga del servidor.

# 6. Atributos flexibles

Como ejemplo, es posible sobreescribir el atributo inicial por uno declarado en el Policyfile.rb. Agregue esto en Policyfile.rb al final:

```
# Policyfile defined attributes
default['base']['message'] = "This node was hardened by Chef. Policyfile created at #{Time.now.utc}\n"
```

Para actualizar el Policyfile.rb con esta modificación:

```
$ kitchen update Policyfile.rb
```

Aplique la convergencia y valide el mensaje del dia:

```
$ kitchen converge centos
```

```
$ kitchen login centos
Last login: Thu Feb  7 18:15:20 2019 from 10.0.2.2
This node was hardened by Chef. Policyfile created at 2019-02-07 18:08:25 UTC
[vagrant@default-centos-7 ~]$ exit
```

Listo, con esto finalizamos los Policyfile. No olvides destruir la VM

```
$ kitchen destroy centos
```
