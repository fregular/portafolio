---
title: "Chef server"
date: 2021-04-22T09:36:21-04:00
draft: false
---

Bien, ahora para continuar explicando otras herramientas como:

* Databag
* Roles
* Nodos clientes
* Subiendo cookbook a chef server

Es necesario contar con un chef server. Adicionalmente, se necesita un chef workstation y un nodo para ejecutar los cookbook. Recomiendo mucho que sigan esta web que explica todo el proceso de instalación de forma muy clara:

https://linuxconfig.org/how-to-install-chef-server-workstation-and-chef-client-on-ubuntu-18-04

Una vez instalado, se puede continuar con este tutorial.

# 1. Comenzamos

En este punto ya tenemos un chef server, un workstation y un nodo con el cliente de chef instalado. Para recordar podemos validar los nodos instalados con este comando:

```
$ knife node list
chef-client-node2
```

El nombre del nodo es el que definieron al momento de instalar el agente. Para obtener mas información del nodo:

```
$ knife node show chef-client-node2
Node Name:   chef-client-node
Environment: _default
FQDN:        chefnode.localdomain.corp
IP:          192.168.122.183
Run List:    recipe[example]
Roles:
Recipes:     
Platform:    ubuntu 18.04
Tags:
```

Bien, ya tenemos un nodo listo para jugar. Ahora ejecutare un comando Ad-hoc.

# 2. Comandos Ad-hoc

Los comandos ad-hoc son instrucciones que podemos ejecutar directamente en el nodo, desde unalinea de comando. Es una buena opción si necesitas configurar algo rapido en el nodo.


Ejemplo:

```
$ chef-run chefnode2.localdomain.corp -i ~/.ssh/id_rsa file hello3.txt content="prueba3"
[✔] Packaging cookbook... done!
[✔] Generating local policyfile... exporting... done!
[✔] Applying file[hello3.txt] from resource to target.
└── [✔] [chefnode2.localdomain.corp] Successfully converged file[hello3.txt].
```

Con esto creamos un archivo hello3.txt con el contenido "prueba3" en el nodo chefnode2.localdomain.corp. Si quisieramos probar una receta, puede ejecutar asi:

```
chef-run chefnode.localdomain.corp -i ~/.ssh/id_rsa webserver.rb
```

En mi caso, las conexiones entre nodos son a traves de llaves, por eso se agrega la bandera "-i" apuntando a la llave privada.

# 3. Crear un repositorio para pruebas

Para crear un repositorio, ejecute este comando:

```
$ chef generate repo chef-repo
$ cd chef-repo/cookbooks/example
```

Crea un recipe de nombre "create_user.rb" en "recipe/" con esta configuración:

```
execute 'disable_swap' do
    command 'swapoff -a'
end

user 'test_user' do
  comment 'usuario por defecto en todos los servidores'
  home '/home/test_user'
  shell '/bin/bash'
  password '$1$bo0Cq88.$usVTH8uuHHi68hakv8G6E1'
end
```

El recipe crea un usuario test_user, con clave "123456", y desabilita la swap. Incluye este recipe en default.rb

```
include_recipe 'example::create_user'
```

# 4. Subiendo un cookbook a chef-server

Para subir un cookbook ejecute este comando

```
$ knife cookbook upload example
Uploading example        [1.0.0]
Uploaded 1 cookbook.
```

Para validar que el cookbook este en chef-server:

```
fregular@chefworkstation2:~/chef-repo$ knife cookbook list
example     1.0.0
```

Bien, para aplicar este cookbook en el nodo, necesitamos obtener el nombre del nodo:

```
$ knife node list
chef-client-node2
```

Ahora agregamos el cookbook example al nodo:

```
$ knife node run_list add chef-client-node2 "recipe[example]"
chef-client-node2:
  run_list:
    recipe[example]
```

Para que el nodo aplique el cookbook, debe ejecutar "chef-client" o puede hacerlos desde su workstation:

```
$ knife ssh chefnode2 "sudo chef-client"
```

Si todo sale bien, el chefnode2 tendre un usuario y la swap desabilitada.

