---
title: "Chef templates"
date: 2021-04-19T16:41:34-04:00
draft: false
---

Siguiendo la serie chef, pasamos a los templates. Los templates son archivos que podemos agregar a nuestra recetas para configurar servicios entre otros caso de uso.

Para la practicar se necesita:

# 1. Prerrequisitos

Para comenzar a jugar, se utilizara todos los componentes en ubuntu 20.04. Para empezar se necesita:

* [Chef Workstation](https://downloads.chef.io/tools/workstation)
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant](https://www.vagrantup.com/downloads)

Una vez instalados, valide que este todo:

```
$ chef --version
```

```
$ VBoxManage --version
```

```
$ vagrant --version
```

Si los comandos se ejecutan sin problema, podemos iniciar con el laboratorio.


# 2. Comenzamos!!

Crea un nuevo cookbook:

```
$ chef generate cookbook web
Generating cookbook web
- Ensuring correct cookbook content
- Committing cookbook files to git

Your cookbook is ready. Type `cd web` to enter it.

There are several commands you can run to get started locally developing and testing your cookbook.
Type `delivery local --help` to see a full list of local testing commands.

Why not start by writing an InSpec test? Tests for the default recipe are stored at:

test/integration/default/default_test.rb

If you'd prefer to dive right in, the default recipe can be found at:

recipes/default.rb
```

```
$ cd web
```

Edite ```recipes/default.rb``` con este contenido:

```
apt_update

package 'apache2'

template '/var/www/html/index.html' do
  source 'index.html.erb'
end

service 'apache2' do
  action [:enable, :start]
end
```

La receta actualiza los repositorios (apt_update), instala el paquete apache2 (package 'apache2'), y utiliza un template y marca como target '/var/www/html/index.html' y el source es 'index.html.erb'. Por ultimo habilita y iniciar el servicio.

Ahora solo queda crear la carpeta templates, y dentro crear el archivo 'index.html.erb' con un contenido de prueba, por ejemplo:

```
<html>
  <head>
    <title>Learn Chef Demo</title>
  </head>
  <body>
    <h1>Hello Learn Chef</h1>
    <p>This is <%=node['hostname']%></p>
  </body>
</html>
```

Ahora solo queda aplicar el cambio con:

```
$ kitchen converge ubuntu
```

Listo, por ultimo valide que la pagina se visualice dentro del la VM ubuntu:

```
$ kitchen login ubuntu
$ curl http://localhost
```

Debe visualizar algo similar a estos:

```
vagrant@default-ubuntu-2004:~$ curl localhost
<html>
  <head>
    <title>Learn Chef Demo</title>
  </head>
  <body>
    <h1>Hello Learn Chef</h1>
    <p>This is default-ubuntu-2004</p>
  </body>
</html>
vagrant@default-ubuntu-2004:~$
```

Genial, los templates son muy utiles cuando necesite aplicar algunas configuraciones base para los diversos software que necesite, tanto para el sistema operativo como para una base de datos, un balanceador de carga y un largooooo etc...
