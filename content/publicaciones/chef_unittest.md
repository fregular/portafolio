---
title: "Chef unittest con kitchen"
date: 2021-04-21T10:13:31-04:00
draft: false
---

# Pruebas unitarias

Las pruebas unitarias son fundamentales para validar que nuestros cookbook estan funcionando correctamente. Es recomentable construir recetas y probarlas con unittest inmediatamente, o utilizar TDD (Test-driven development)


# 1. Prerrequisitos

Se reutilizara el repositorio de chef utilizado en [chef template](https://fregular.gitlab.io/portafolio/publicaciones/chef_templates/) y tambien [chef_attributes](https://fregular.gitlab.io/portafolio/publicaciones/chef_attributes).

# 2. Comenzamos

Para ir haciendo test unitarias, podemos iniciar probando los test con el comando "kitchen", para ello se existe el directorio ```test/integration/default/default_test.rb```. En este archivo podemos definir pruebas unitarias que correran al ejecutar el comando:

```
$ kitchen test
```

**Nota: En esta guia solo ejecutaremos los test en el servidor Ubuntu.**

Este comando ejecutar el run_list definido en el Policyfile.rb (en los siguientes post se hablará de los Policy), en este caso es la receta "default.rb". Para ejemplificar, agregue estas configuraciones:

1- crear el archivo  haproxy.rb en /recipe, con el siguiente contenido:

```
haproxy.rb
apt_update
package 'haproxy'

directory '/etc/haproxy'


service 'haproxy' do
    action [:enable, :start]
end
```

2- Incluya la receta haproxy en default.rb:

```
#
# Cookbook:: web
# Recipe:: default
#
# Copyright:: 2021, The Authors, All Rights Reserved.
apt_update

package 'apache2'

template '/var/www/html/index.html' do
  source 'index.html.erb'
end

service 'apache2' do
  action [:enable, :start]
end


file '/etc/motd' do
  content node['base']['message']
end

include_recipe 'web::haproxy'
```

Recuerde que 'web::haproxy' hace referencia al cookbook (web) y la receta (haproxy). Ahora agregaremos los test unitarios en ```test/integration/default/default_test.rb```.

```
# InSpec test for recipe learn_chef::default

# The InSpec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec/resources/

unless os.windows?
  # This is an example test, replace with your own test.
  describe user('root'), :skip do
    it { should exist }
  end
end

# This is an example test, replace it with your own test.
describe port(80), :skip do
  it { should_not be_listening }
end


describe port(80) do
  it { should be_listening }
end

describe package('apache2') do
  it { should be_installed }
end

describe package('haproxy') do
  it { should be_installed }
end
```

Si se fijan, las dos primeras pruebas no se ejecutan (skip). Las 3 ultimas pruebas se ejecutan, de estas la primera valida si el puerto 80 esta abierto, la segunda y tercera valida si el paquete apacje2 y haproxy esta instalado. Si estas pruebas fallan, la salida del comando entregara la información. Si ejecuta:

```
$ kitchen test ubuntu
```

Tendra una salida como esta:

```
Target:  ssh://vagrant@127.0.0.1:2222

  User root
     ↺
  Port 80
     ↺
  Port 80
     ✔  is expected to be listening
  System Package apache2
     ✔  is expected to be installed
  System Package haproxy
     ✔  is expected to be installed

Test Summary: 3 successful, 0 failures, 2 skipped
       Finished verifying <default-ubuntu-2004> (0m2.04s).
-----> Destroying <default-ubuntu-2004>...
       ==> default: Forcing shutdown of VM...
       ==> default: Destroying VM and associated drives...
       Vagrant instance <default-ubuntu-2004> destroyed.
       Finished destroying <default-ubuntu-2004> (0m4.40s).
       Finished testing <default-ubuntu-2004> (1m34.46s).
-----> Test Kitchen is finished. (1m35.17s)
```

Las prueba unitarias se ejecutan, y una vez termina la prueba, se elimina la virtual machine. Esta prueba es rapida y ayuda a validar rapidamente si nuestros cookbook hacen lo que tienen que hacer.


