#!/bin/bash

OS=$(cat /etc/issue)
mem=$(cat /proc/meminfo |grep MemTo|awk '{print $2}')
cpu_info=$(cat /proc/cpuinfo |grep "model name"|head -1| cut -d: -f2)

echo $OS;
echo $mem;
echo $cpu_info;
