---
title: "Sobre mi"
date: 2021-01-17T11:27:17-03:00
draft: false
---

Soy Ingeniero de Sistema con amplia experiencia en Retail. Me gusta estudiar de todo, siento que hay tanto que aprender que me faltan horas del día para seguir. Estoy interesado en la cultura devops, codear y automatizar todo lo que se pueda. Soy fanático de las plataformas de e-learning como Platzi, Udemy.

Habilidades:
* Scripting (bash, python).
* Administración de servidores (Redhat, Centos, Suse, Debian).
* Servicios como: DNS, NFS, Samba, Nginx.
* Framework en Django y Flask de forma basica (backend).
* Administrador de logs (ELK Stack).
* Administración de la configuración con Ansible.
* Tecnología de contenedores como Docker y orquestación con Kubernetes.
* Herramientas CI/CD como Jenkins o Gitlab.
* Git.
